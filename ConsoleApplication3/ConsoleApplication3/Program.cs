﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            CircularLinkedList<int> cll = new CircularLinkedList<int>(new List<int>() { 6, 7, 1, 2, 2, 3, 4, 4, 1, 5 });

            Console.WriteLine("Исходный список:");
            Console.WriteLine(cll.ToString());
            Console.WriteLine(new string('-', 30));

            //Добавление и удаление элементов
            cll.AddFirst(2);
            cll.AddLast(3);
            var node2 = cll.Find(2);
            cll.AddAfter(node2, 3);
            cll.AddLast(4);
            cll.RemoveFirst();
            cll.Remove(3);
            cll.RemoveLast();

            Console.WriteLine("Исходный список после изменения:");
            Console.WriteLine(cll.ToString());
            Console.WriteLine(new string('-', 30));

            Console.WriteLine("Отсортированный список");
            cll.Sort();
            Console.WriteLine(cll.ToString());
            Console.WriteLine(new string('-', 30));

            Console.WriteLine("Поиск элемента 2");
            Console.WriteLine("Индекс: " + cll.BinarySearch(2));
            Console.ReadKey();
        }
    }
}
