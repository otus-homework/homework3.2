﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApplication3
{
    public interface IAlgorithm<T>
    {
        void Sort();
        int BinarySearch(T searchedValue);
    }
}
