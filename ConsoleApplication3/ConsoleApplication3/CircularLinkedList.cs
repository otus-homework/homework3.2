﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApplication3
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }

        public Node<T> Next { get; set; }
        public T Data { get; set; }
    }

    public class CircularLinkedList<T> : IEnumerable<T>, IAlgorithm<T>
    {
        private Node<T> _head;
        private Node<T> _tail;
        private int _count;

        public CircularLinkedList() { }

        public CircularLinkedList(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            foreach (T item in collection)
            {
                AddLast(item);
            }
        }

        public Node<T> First
        {
            get { return _head; }
        }

        public Node<T> Last
        {
            get { return _head == null ? null : _tail; }
        }

        public int Count
        {
            get { return _count; }
        }

        public bool IsEmpty
        {
            get { return _count == 0; }
        }

        #region Add

        public void AddFirst(T value)
        {
            AddFirst(new Node<T>(value));
        }

        public void AddFirst(Node<T> newNode)
        {
            ValidateNode(newNode);

            if (_head == null)
            {
                InsertNodeToEmptyList(newNode);
            }
            else
            {                
                InsertNodeAfter(_tail, newNode);
                _head = newNode;
            }
        }

        public void AddLast(T value)
        {
            AddLast(new Node<T>(value));
        }

        public void AddLast(Node<T> newNode)
        {
            ValidateNode(newNode);

            if (_head == null)
            {
                InsertNodeToEmptyList(newNode);
            }
            else
            {                
                InsertNodeAfter(_tail, newNode);
                _tail = newNode;
            }

        }

        public void AddAfter(Node<T> node, T value)
        {
            ValidateNode(node);

            AddAfter(node, new Node<T>(value));
        }

        public void AddAfter(Node<T> node, Node<T> newNode)
        {
            ValidateNode(newNode);

            InsertNodeAfter(node, newNode);
        }

        #endregion

        #region Remove

        public bool Remove(T value)
        {
            return Remove(Find(value));
        }

        public bool Remove(Node<T> removedNode)
        {
            ValidateList();

            if (removedNode != null)
            {
                if (_count == 1)
                {
                    Clear();
                    return true;
                }
                if (removedNode == _head)
                {
                    return RemoveFirst();
                }
                if (removedNode == _tail)
                {
                    return RemoveLast();
                }
                Node<T> previousNode = FindPreviousNode(removedNode);
                previousNode.Next = removedNode.Next;
                _count--;
                return true;
            }
            return false;
        }


        public bool RemoveFirst()
        {
            ValidateList();

            if (_count == 1)
            {
                Clear();
            }
            else
            {
                _head = _head.Next;
                _tail.Next = _head;
                _count--;
            }
            return true;
        }

        public bool RemoveLast()
        {
            ValidateList();

            if (_count == 1)
            {
                Clear();
            }
            else
            {
                Node<T> previousNode = FindPreviousNode(_tail);
                _tail = previousNode;
                _tail.Next = _head;
                _count--;
            }
            return true;
        }

        #endregion

        public Node<T> Find(T value)
        {
            Node<T> current = _head;
            EqualityComparer<T> c = EqualityComparer<T>.Default;
            if (current != null)
            {
                if (value != null)
                {
                    do
                    {
                        if (c.Equals(current.Data, value))
                        {
                            return current;
                        }
                        current = current.Next;
                    } while (current != _head);
                }
                else
                {
                    do
                    {
                        if (current.Data == null)
                        {
                            return current;
                        }
                        current = current.Next;
                    } while (current != _head);
                }
            }
            return null;
        }

        public bool Contains(T value)
        {
            if (Find(value) == null)
                return false;
            return true;
        }

        public void Clear()
        {
            _head = null;
            _tail = null;
            _count = 0;
        }

        public override string ToString()
        {
            return String.Join(", ", this);
        }

        #region Вспомогательные методы

        private Node<T> FindPreviousNode(Node<T> curr)
        {
            Node<T> prev = _head;
            do
            {
                prev = prev.Next;
            }
            while (prev.Next != curr);
            return prev;
        }

        private void InsertNodeToEmptyList(Node<T> node)
        {
            _head = node;
            _tail = node;
            _tail.Next = _head;
            _count++;
        }

        private void InsertNodeAfter(Node<T> node, Node<T> newNode)
        {
            newNode.Next = node.Next;
            node.Next = newNode;
            _count++;
        }

        private void ValidateNode(Node<T> node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node is null ");
            }
        }

        private void ValidateList()
        {
            if (_head == null)
            {
                throw new InvalidOperationException("list is empty");
            }
        }

        #endregion

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = _head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != _head);
        }

        #endregion

        #region IAlgorithm

        public void Sort()
        {
            Comparer<T> c = Comparer<T>.Default;

            //кол-во раз обмена местами
            int swapCount;
            do
            {
                Node<T> curr = _head;
                swapCount = 0;
                do
                {
                    if (c.Compare(curr.Data, curr.Next.Data) == 1)
                    {
                        SwapToNext(curr, curr.Next);
                        swapCount++;
                    }
                    curr = curr.Next;
                } while (curr.Next != _head);

            } while (swapCount != 0);

        }

        private void SwapToNext(Node<T> curr, Node<T> next)
        {
            if (_count == 2)
            {
                _head = next;
                _tail = curr;
                return;
            }

            Node<T> prev = FindPreviousNode(curr);
            prev.Next = next;
            curr.Next = next.Next;
            next.Next = curr;
            if (curr == _head)
            {
                _head = next;
            }
            if (next == _tail)
            {
                _tail = curr;
            }
        }

        public int BinarySearch(T searchedValue)
        {
            int left = 1;
            int right = _count;
            EqualityComparer<T> ec = EqualityComparer<T>.Default;
            Comparer<T> c = Comparer<T>.Default;

            while (left <= right)
            {
                //индекс среднего элемента
                var middle = (left + right) / 2;
                var middleValue = FindElementByIndex(middle);
                if (ec.Equals(searchedValue, middleValue))
                {
                    return middle;
                }
                else if (c.Compare(searchedValue, middleValue) == 1)
                {
                    //сужаем рабочую зону массива с правой стороны
                    right = middle - 1;
                }
                else
                {
                    //сужаем рабочую зону массива с левой стороны
                    left = middle + 1;
                }
            }
            //ничего не нашли
            return -1;
        }

        private T FindElementByIndex(int index)
        {
            ValidateList();
            int currIndex = 1;
            Node<T> current = _head;

            if (index == 1)
                return _head.Data;

            do
            {
                currIndex++;
                current = current.Next;
            } while (currIndex != index);
            return current.Data;
        }

        #endregion
    }
}
